

from __future__ import print_function

from flask import Flask, render_template
from jinja2 import Template
from flask_sqlalchemy import SQLAlchemy

from os import path
import sys
import io

from models import db



app = Flask(__name__)
app.config['DEBUG'] = True
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config["CACHE_TYPE"] = "null"

#app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://DB_USER:postgres@postgres/test'


POSTGRES = {
    'user': 'postgres',
    'pw': 'postgres',
    'db': 'professional_maintenance',
    'host': 'localhost',
    'port': '5432',
}
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://%(user)s:\
%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES

db.init_app(app)


# change to "redis" and restart to cache again

d = path.dirname(__file__)

#nltk.download('punkt')

@app.route('/1')
def test1():
	print(dir(db))
	return render_template('test.html')

@app.route('/2')
def test2():
    return render_template('test2.html')
'''
@app.route('/leap_stuff')
def leap():
    return render_template('data_samplig.html')

@app.route('/content_analysis')
def content_analysis():
    #fp = open("as_a_man_thinketh.txt")
    #data = fp.read()
    # Read the whole text.
    #text = open(path.join(d, 'bibles.txt')).read()
    
    contents = "";
    #file_name = 'pg10.txt' 
    file_name = 'static/as_a_man_thinketh.txt' 

    with io.open(path.join(d, file_name), 'r',   errors = 'ignore') as f:
        contents = f.read()
        ##USING STOP WORDS
        words = word_tokenize(contents)
        wordsFiltered = "" 
        for w in words:
            if w not in stopWords:
                wordsFiltered += w +" "
         
        #print(wordsFiltered)
        wordcloud = WordCloud().generate(wordsFiltered)
    


    image = wordcloud.to_image() 
    image.save(path.join(d,'static/TEMP.jpeg'))

    sentences= nltk.sent_tokenize(contents)
    print(len(sentences))
    
   
    responses = [];
    i = 0
    for sentence in sentences:
        print(i)
        responses.append(json.dumps(tone_analyzer.tone(tone_input=sentence, content_type="text/plain", content_language='es'), indent=2))
        i+=1
    print(responses)
   
    
    return render_template('test.html',result = sentences)
 '''

import os
if __name__ == "__main__":
    # Only for debugging while developing
    #app.run(host='0.0.0.0', debug=True, port=80) 
    app.run(host=os.getenv('IP', '0.0.0.0'),port=int(os.getenv('PORT', 1234)))











