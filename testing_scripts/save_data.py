#!/usr/bin/env python
#
# GrovePi Example for using the Grove Sound Sensor and the Grove LED
#
# The GrovePi connects the Raspberry Pi and Grove sensors.  You can learn more about GrovePi here:  http://www.dexterindustries.com/GrovePi
#
# Modules:
#	 http://www.seeedstudio.com/wiki/Grove_-_Sound_Sensor
#	 http://www.seeedstudio.com/wiki/Grove_-_LED_Socket_Kit
#
# Have a question about this example?  Ask on the forums here:  http://forum.dexterindustries.com/c/grovepi
#
'''
## License

The MIT License (MIT)

GrovePi for the Raspberry Pi: an open source platform for connecting Grove Sensors to the Raspberry Pi.
Copyright (C) 2017  Dexter Industries

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
'''

import time
import grovepi
import numpy as np
import matplotlib.pyplot as plt
import datetime


#plt.axis([0, 1000, 0, 2])

def get_db():
    from pymongo import MongoClient
    client = MongoClient('localhost:27017')
    db = client.sensor_data
    return db

def add_data(db,data):
    now = datetime.datetime.now()
    print(now)
    to_send = {
                "time_stamp" : now,
                #"sound_sensor" : data,
                "button_signal" : data, 
              }
    db.sensors.insert(to_send)

def get_data(db):
    return db.sensors.count()

db = get_db() 

# Connect the Grove Sound Sensor to analog port A0
# SIG,NC,VCC,GND
sound_sensor = 0

button = 6
# Connect the Grove LED to digital port D5
# SIG,NC,VCC,GND
led = 5

buzzer = 8

grovepi.pinMode(sound_sensor,"INPUT")

grovepi.pinMode(button,"INPUT")

grovepi.pinMode(led,"OUTPUT")

grovepi.pinMode(buzzer,"OUTPUT")
grovepi.digitalWrite(buzzer,0)
# The threshold to turn the led on 400.00 * 5 / 1024 = 1.95v
threshold_value = 400

to_send = []
counter = 0
data_amount = 1000
y=[]
x=[]
while True:
    try:
        grovepi.digitalWrite(led,0)
        # Read the sound level
        #sensor_value = grovepi.analogRead(sound_sensor)
        #print("sensor_value = %d" %sensor_value)
        #time.sleep(.5)  
        #if grovepi.digitalRead(button) == 1:
        button_value = grovepi.digitalRead(button)
        #print(button_value)
        #grovepi.digitalWrite(led,1)
        #temp = [sensor_value,button_value]
        #temp = [0,button_value]
        to_send.append(button_value)
        counter +=1 
        #print(len(to_send))
        print(counter)
        #y.append(sensor_value)
        #y.append(button_value)
        if (counter > data_amount):
            
            grovepi.digitalWrite(buzzer,1)
            grovepi.digitalWrite(led,1)
            time.sleep(0.0005)
            grovepi.digitalWrite(buzzer,0)
            grovepi.digitalWrite(led,0)
            #plt.ylim(0, 2)
            #plt.plot(range(0,len(y)),y)
            #plt.show()
            #y.clear()
            ####################################
            ##sending data batch to DB 
            
            add_data(db,to_send)
            #print (get_data(db))


            ####################################

            print("DATA SAVED")
            
            #time.sleep(2)
            counter = 0
            to_send.clear()  
        

    except KeyboardInterrupt:
        grovepi.digitalWrite(buzzer,0)
        grovepi.digitalWrite(led,0)    
        break
    except IOError:
        print ("Error")

