import time
import datetime
import threading
import random
from bson.json_util import dumps

#A bunch of import statements which you should ignore for now
from flask import Flask, render_template,  request,jsonify
from flask_socketio import SocketIO, emit,send
 
from flask_sqlalchemy import SQLAlchemy
from os import path
import sys
import io

from models import sqldb


# change to "redis" and restart to cache again

d = path.dirname(__file__)

app = Flask(__name__)
app.config['SECRET_KEY'] = 'mysecret'
app.config['DEBUG'] = True
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config["CACHE_TYPE"] = "null"



####NON SQL PART (MONGODB)

def get_db():
    from pymongo import MongoClient
    client = MongoClient('localhost:27017')
    db = client.sensor_data
    return db

def add_data(db,data):
    now = datetime.datetime.now()
    print(now)
    to_send = {
                'time_stamp' : now,
                #"sound_sensor" : data,
                'signals' : data, 
              }
    db.sensors.insert(to_send)  

def get_data(db):
    to_show= []
    counter = 0
    for i in db.sensors.find():
        to_show.append(dumps(i)) 
        counter += 1 
    return to_show 
    #db.sensors.count()

db = get_db() 


class TestThreading(object):
    
    
    def __init__(self, interval=0.5):
        self.interval = interval
        self.data = 0
        self.to_send = []
        self.counter = 0
        self.data_amount = 10
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        while True:
            # More statements comes here
            #self.data = datetime.datetime.now().__str__()
                           
            self.data = random.randint(0,200)
            self.counter +=1
            print(self.counter)
            self.to_send.append(self.data)
            if (self.counter >= self.data_amount):
                add_data(db,self.to_send)
                self.counter = 0
                self.to_send.clear()
            time.sleep(self.interval)

tr = TestThreading()

POSTGRES = {
    'user': 'postgres',
    'pw': 'postgres',
    'db': 'professional_maintenance',
    'host': 'localhost',
    'port': '5432',
}

#app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://DB_USER:postgres@postgres/test'

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://%(user)s:\
%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES

sqldb.init_app(app)

socketio = SocketIO(app)

@socketio.on('message')
def handleMessage(msg):
	print('Mesage: '+ msg)
	send(msg,broadcast=True)

@socketio.on('get_data')
def send_info():
	send(str(tr.data),broadcast=True)

@socketio.on('my_ping')
def ping_pong():
    emit('my_pong')

    
@app.route('/')
def test1():
    return render_template('index.html')

#/sockets
@app.route('/1')
def root():
    return render_template('sockets.html')
#/tree
@app.route('/2')
def test2():
    print (get_data(db))
    return render_template('demo.html')
#/json
@app.route("/3")
def geo_code():
    
    test = {'test_key':get_data(db)}
    return jsonify(test)

if __name__ == '__main__':
    socketio.run(app)
