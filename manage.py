import flask
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from test import app, sqldb


migrate = Migrate(app, sqldb)
manager = Manager(app)

manager.add_command('db', MigrateCommand)


if __name__ == '__main__':
    manager.run()