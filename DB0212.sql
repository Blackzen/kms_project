﻿CRCREATE DATABASE IF NOT EXISTS `webmvcframework10` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `webmvcframework10`;
-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: webmvcframework10
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `attribute`
--

DROP TABLE IF EXISTS `attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `attribute` (
  `PK_attribute` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`PK_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute`
--

LOCK TABLES `attribute` WRITE;
/*!40000 ALTER TABLE `attribute` DISABLE KEYS */;
INSERT INTO `attribute` VALUES (1,'attr1'),(2,'attr2'),(3,'attr3'),(4,'attr4'),(5,'attr5'),(6,'attr6'),(7,'attr7'),(8,'attr8'),(9,'attr9'),(10,'attr10');
/*!40000 ALTER TABLE `attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `part`
--

DROP TABLE IF EXISTS `part`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `part` (
  `PK_part` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `parent_part_id` int(11) NOT NULL,
  PRIMARY KEY (`PK_part`,`parent_part_id`),
  UNIQUE KEY `PK_part_UNIQUE` (`PK_part`),
  KEY `fk_part_part1_idx` (`parent_part_id`),
  CONSTRAINT `fk_part_part1` FOREIGN KEY (`parent_part_id`) REFERENCES `part` (`pk_part`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `part`
--

LOCK TABLES `part` WRITE;
/*!40000 ALTER TABLE `part` DISABLE KEYS */;
/*!40000 ALTER TABLE `part` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `part_attributes`
--

DROP TABLE IF EXISTS `part_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `part_attributes` (
  `part_PK_part` int(11) NOT NULL,
  `attribute_PK_attribute` int(11) NOT NULL,
  PRIMARY KEY (`part_PK_part`,`attribute_PK_attribute`),
  KEY `fk_part_has_attribute_attribute1_idx` (`attribute_PK_attribute`),
  KEY `fk_part_has_attribute_part1_idx` (`part_PK_part`),
  CONSTRAINT `fk_part_has_attribute_attribute1` FOREIGN KEY (`attribute_PK_attribute`) REFERENCES `attribute` (`pk_attribute`),
  CONSTRAINT `fk_part_has_attribute_part1` FOREIGN KEY (`part_PK_part`) REFERENCES `part` (`pk_part`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `part_attributes`
--

LOCK TABLES `part_attributes` WRITE;
/*!40000 ALTER TABLE `part_attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `part_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `part_type`
--

DROP TABLE IF EXISTS `part_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `part_type` (
  `PK_part_type` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `part_PK_part` int(11) DEFAULT NULL,
  PRIMARY KEY (`PK_part_type`),
  KEY `fk_part_type_part_idx` (`part_PK_part`),
  CONSTRAINT `fk_part_type_part` FOREIGN KEY (`part_PK_part`) REFERENCES `part` (`pk_part`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `part_type`
--

LOCK TABLES `part_type` WRITE;
/*!40000 ALTER TABLE `part_type` DISABLE KEYS */;
INSERT INTO `part_type` VALUES (1,'parttype1',NULL),(2,'parttype2',NULL),(3,'parttype3',NULL),(4,'parttype4',NULL),(5,'parttype5',NULL),(6,'parttype6',NULL),(7,'parttype7',NULL),(8,'parttype8',NULL),(9,'parttype9',NULL),(10,'parttype10',NULL);
/*!40000 ALTER TABLE `part_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `part_types_attributes`
--

DROP TABLE IF EXISTS `part_types_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `part_types_attributes` (
  `part_type_PK_part_type` int(11) NOT NULL,
  `attribute_PK_attribute` int(11) NOT NULL,
  PRIMARY KEY (`part_type_PK_part_type`,`attribute_PK_attribute`),
  KEY `fk_part_type_has_attribute_attribute1_idx` (`attribute_PK_attribute`),
  KEY `fk_part_type_has_attribute_part_type1_idx` (`part_type_PK_part_type`),
  CONSTRAINT `fk_part_type_has_attribute_attribute1` FOREIGN KEY (`attribute_PK_attribute`) REFERENCES `attribute` (`pk_attribute`),
  CONSTRAINT `fk_part_type_has_attribute_part_type1` FOREIGN KEY (`part_type_PK_part_type`) REFERENCES `part_type` (`pk_part_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `part_types_attributes`
--

LOCK TABLES `part_types_attributes` WRITE;
/*!40000 ALTER TABLE `part_types_attributes` DISABLE KEYS */;
INSERT INTO `part_types_attributes` VALUES (2,1),(3,2),(6,2),(8,2),(10,2),(5,3),(1,4),(4,4),(7,4),(9,4),(1,5),(10,7),(2,8),(3,8),(7,8),(9,8),(4,10),(5,10),(6,10),(8,10);
/*!40000 ALTER TABLE `part_types_attributes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-02 16:21:46

